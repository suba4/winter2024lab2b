import java.util.Scanner;
public class GameLauncher {
	
	public static void main(String[] arg){
		Scanner reader = new Scanner (System.in);
		//if user types one it will run Hangman, Wordle if number 2 is inputed)
		
		System.out.println("Type 1 if you would like to play Hangman, 2 if you would like to play Wordle");
		//Store the value given by the user
		int game = reader.nextInt();
		
		//If user inputs another number other than 1 or 2 they are asked to give a number that is either 1 or 2.
		while(game !=1 && game!= 2){
		System.out.println("Please type 1 if you would like to play Hangman, 2 if you would like to play Wordle");
		game = reader.nextInt();
		}
			
		//if they inputed 1 it would run Hangman
		if(game == 1){
			//Get user input
			System.out.println("Enter a 4-letter word:");
			String word = reader.next();
			//Convert to upper case
			word = word.toUpperCase();
		
			//Start hangman game
			Hangman.runGame(word);	
		
		}
		//if it was not 1, and it could not be any number other than 1 or 2, it would mean the else statement would be for when the user had inputed the number 2
		else {
			String answer = Wordle.generateWord();
			Wordle.runGame(answer);
		}

		
		
	}
	
	
}

