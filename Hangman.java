import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	for (int i = 0; i<4 ; i++){
	
		if (word.charAt(i)==c){
			return i;
		}
			
	}
	   return -1;
}
	public static char toUpperCase(char c) {
		return Character.toUpperCase(c);
		// instantly returns c after changing it to uppercase
	}
	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
		String newWord = "";
		if (letter0){
			newWord+= word.charAt(0);
			// checks if the guessed letter is the first letter of the word
			// if yes it print the first letter on the terminal
		}
		else {
			newWord+=" _ ";
			//if not it keeps the first slot of the word in the terminal empty
		}
		//same thing is done just like for the first slot of the word, for the other 3 letters
		if (letter1){
			newWord+= word.charAt(1);
			
		}
		else {
			newWord+=" _ ";
			
		}
		if (letter2){
			newWord+= word.charAt(2);
			
		}
		else {
			newWord+=" _ ";
		}

		if (letter3){
			newWord+=word.charAt(3);
			
		}
		else {
			newWord+=" _ ";
		}
		System.out.println("Your result is "+ newWord);
	}
	public static  void runGame(String word){
		boolean letter0 = false;
		boolean letter1= false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuess = 0;
	
		
		while(numberOfGuess < 6 && !(letter0 && letter1 && letter2 && letter3)){

			Scanner reader = new Scanner (System.in);
			
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess =toUpperCase(guess);
			
			if(isLetterInWord(word,guess) == 0)
				letter0 = true;
			if(isLetterInWord(word,guess) == 1)
				letter1 = true;
			if(isLetterInWord(word,guess) == 2)
				letter2 = true;
			if(isLetterInWord(word,guess) == 3)
				letter3 = true;
			else
				numberOfGuess++;
			// if the letter is not in any of the other slots we know that we need to immediately add +1 to the numberofGuess
			
			printWork(word, letter0, letter1, letter2, letter3);
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		else
			System.out.println("Congrats! You got it :");
		//if you did not get 6 mistakes and the code goes out of the loop. It means that you have guessed all the letters correctly, which means we can just print the winning message
		
	}
	
}